package br.ucsal.bes20182.testequalidade.restaurante.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class ComandaUnitarioTest {

	/**
	 * M�todo a ser testado: private Double calcularTotal(). Verificar o c�lculo do
	 * valor total, com um total de 4 itens.
	 */

	private ArrayList<Item> itens;
	private Item item;

	@Before
	public void setup() {
		item = new Item("caju", 10d);
		itens.add(item);
		item = new Item("caja", 20d);
		itens.add(item);
		item = new Item("banana", 30d);
		itens.add(item);
		item = new Item("manga", 40d);
		itens.add(item);

	}

	@Test
	public void calcularTotal4Itens() {
		double result = 0;
		double expected = 100.d;

		for (int i = 0; i < 3; i++) {
			item = itens.get(i);
			result += item.valorUnitario;
		}
		try {

			Comanda comandaMock = PowerMockito.spy(new Comanda(new Mesa(1)));

			PowerMockito.when(comandaMock, "calcularTotal").thenReturn(new Double(100));

			result = comandaMock.fechar();

			PowerMockito.verifyPrivate(comandaMock).invoke("calcularTotal");

			assertEquals(expected, result);

		} catch (Exception e) {

		}

	}

}
